# Software Studio 2021 Spring Midterm Project
## Topic
* Project Name : [Midterm_Project_Chatroom]

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Membership Mechanism|15%|Y|
|Firebase Page|5%|Y|
|Database|15%|Y|
|RWD|15%|Y|
|Topic Key Function|20%|Y|

## Advanced Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Third-Party Sign In|2.5%|Y|
|Chrome Notification|5%|Y|
|Use CSS Animation|2.5%|Y|
|Security Report|5%|Y|
|change chatroom background image||Y|
|change user headshot||Y|
|change user name||Y|
|show last message info on chatroom list||Y|

# 作品網址：[https://hw-2-69a48.firebaseapp.com]

## Website Detail Description
[只可以用...@gmail.com的形式註冊]

[database structure]
* chatroom
    * roomID
        * bgimgURL
        * content
            * messageID(hash)
                * date
                * email
                * message                
* user
    * userID(由gmail生成, eg.123@gmail.com => id = 123)
        * roomID
* user2
    * userID(hash)
        * email
        * headshot url
        * name


# Components Description : 
1. [menu] : [點personal setting可以改名稱和頭貼]
2. [chatroom]] : [打字按send送出訊息]
3. [chatroom list] : [左邊那條, 點別的房間可以跳過去]
4. [add chatroom] : [在左邊那條最下方, 點了可以建房間, 但房間名不能為"public"]
5. [chatroom setting] : [在chatroom左上方, 點了可以邀人(要輸入email), 和設定聊天室的背景圖片]

# Other Functions Description : 
1. [onAuthStateChanged] : [處理登入初始化, 首先設定使用者資訊, 存在就fetch user2的id, 不存在就建id, 第二是初始化menu, 第三是根據user的id找使用者在的房間, 並把它們加入chatroom list]
2. [createNotification] : [根據input做一個notification]
3. [change chat] : [換聊天室時的function, 它會先關掉舊的listener, 再去追蹤新的'content'和'bgimgUrl', 以便更新聊天室內容]
4. [initChatList] : [去user抓取使用者在的所有房間id, 以id去chatroom fetch他們的最後一筆資訊, 並顯示在chatroom list]
5. [updateChatList] : [追蹤聊天室的'content', 去更新chatroom list的內容(advanced component的'show last message info on chatroom list'), 它做的事情是去每個聊天室取最後一筆資訊, 然後更新chatroom list]
6. [getUserID] : [抓取使用者的user2 id, 回傳一個promise]
7. [sendBtn的click listener] : [push input message到現在聊天室的content下]
8. [newChatBtn的click listener] : [把input(聊天室名字)push到現在使用者的user底下]
9. [nameInputBtn的click listener] : [update user2下的name]
7. [headshotInputBtn的click listener] : [update user2下的headshot]

