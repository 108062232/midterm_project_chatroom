window.onload = function() {
    var email = document.getElementById('mail');
    var password = document.getElementById('password');
    var provider = new firebase.auth.GoogleAuthProvider();

    document.getElementById('googleLoginBtn').onclick = function() {
        firebase.auth().signInWithPopup(provider)
        .then(passMessage=>{
            window.location.href = "index.html";
        })
        .catch(error=>{
            window.alert(error.message);
        });
    }

    document.getElementById('signUpBtn').onclick = function() {
        console.log('click1');

        firebase.auth().createUserWithEmailAndPassword(email.value, password.value).then(
            (passMessage)=>{console.log(passMessage); console.log('create success');}, 
            (error)=>{console.log(error.code + ' ' + error.message + ' failed')}
        );
        
    }

    document.getElementById('loginBtn').onclick = function() {
        console.log('click2');

        firebase.auth().signInWithEmailAndPassword(email.value, password.value).then(
            (passMessage)=>{
                window.location.href = "index.html";
            }, 
            (error)=>{
                console.log(error.code + ' ' + error.message + ' failed')
            }
        ); 
    }

};

