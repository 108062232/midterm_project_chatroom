var chatroomRef;
var chatroomContent;
var chatListener, chatListener2;
var chatroomList;

window.onload = function() {
    var sendBtn = document.getElementById('sendBtn');
    var newChatBtn = document.getElementById('newChatBtn');
    var message = document.getElementById('message');
    var nameInputBtn = document.getElementById('nameInputBtn');
    var headshotInputBtn = document.getElementById('headshotInputBtn');
    var user_email = '';
    var userID = '';
    var user2ID = '';
    
    chatroomRef = firebase.database().ref('chatroom/public');
    chatroomContent = document.getElementById('chatroomContent');
    chatroomList = document.getElementById('chatroomList');

    // handle sign in/out and record current email
    firebase.auth().onAuthStateChanged(function(user) {
        var menu = document.getElementById('menu');

        if (user) {
            user_email = user.email;

            // ID = email - '@gmail.com'
            userID = user_email.substr(0, user_email.length - 10);
            // console.log(userID);

            createNotification('welcome to chat!')

            // init user data
            firebase.database().ref('user2/').orderByChild('email').equalTo(user_email).once('value', snapshot=>{
                if(!snapshot.exists()){
                    user2ID = firebase.database().ref('user2/').push({
                        email : user_email,
                        userName : userID,
                        headshot : '../img/person.jpg'
                    }).key;
                }
                else {
                    snapshot.forEach(element=>{
                        user2ID = element.key;
                        // console.log(user2ID);
                    })
                }
            });

            

            
            menu.innerHTML =
            '<h5 class="m-2" id="animatedTitle">hw2 chatroom</h5>'+
			'<div class="dropdown">'+
				'<button type="button" class="btn btn-primary dropdown-toggle m-3" data-toggle="dropdown">menu</button>'+
				'<div class="dropdown-menu dropdown-menu-right bg-dark">'+
					'<h5 class="dropdown-header text-center">'+ user.email +'</h5>'+
                    '<div class="dropdown-divider"></div>'+
			    	'<button type="button" class="dropdown-item btn bg-dark text-white text-center" data-toggle="modal" data-target="#ppModal">personal setting</button>'+
			    	'<button type="button" class="dropdown-item bg-dark text-white text-center" id = "logOutBtn">Logout</button>' + 				
				'</div>'+
			'</div>'; 

            document.getElementById('logOutBtn').addEventListener('click', function(){
                firebase.auth().signOut()
                .then(function(){  alert('logout success'); window.location.href = "login.html"; })
                .catch(function(){ alert('logout failed'); });     
            });


            // init chatlist
            // chatlist trace for public
            updateChatList('public');  
            // traverse the whole chat that user is in, loop each room
            firebase.database().ref('user/' + userID).once('value', snapshot=>{
                snapshot.forEach( element=>{

                    // get room name
                    firebase.database().ref('chatroom/' + element.val() + '/roomName').once('value', item=>{
                        var roomName = item.val();

                        // // fetch last message to initalize chatlist
                        // initChatList(element.val(), roomName);                       

                        // // trace each room to update chatlsit
                        // updateChatList(element.val());                    

                        initChatList(element.val(), roomName).then(()=>{
                            updateChatList(element.val());
                        });
                    });
                });
            })
            .then(()=>{
                // error
                // update chatlist if userName is changed, but have to wait till the chatlist is all initialized
                firebase.database().ref('user2/' + user2ID).on('value', ()=>{
                    updateChatList('public');

                    firebase.database().ref('user/' + userID).once('value', snapshot=>{
                        snapshot.forEach(element=>{
                            updateChatList(element.val());
                        });
                    }); 
                });
            }) 
            
            
              
        } else {
            window.location.href = "login.html";
        }
    });

    // send message to server
    sendBtn.addEventListener('click', function() {
        if (message.value != "") {
            
            // quick fix for displaying html code
            var data = message.value.replace(/</g, "&lt;");

            var now = new Date();
            chatroomRef.child('content').push({
                email: user_email,
                message : data,
                date: {
                    month : now.getMonth() + 1,
                    day : now.getDate(),
                    hour : now.getHours(),
                    min : now.getMinutes() + 1
                }
            });
            message.value = '';
        }
    });
    
    // invite ppl
    inviteBtn.addEventListener('click', function() {
        var userName = window.prompt("please input the the email you want to invite", "");
        userName = userName.substr(0, userName.length - 10);

        if(userName != null && userName != ''){
            chatroomRef.get().then((snapshot)=>{
                firebase.database().ref('user/' + userName).push(snapshot.key);
            })
        }
    });


    // new a chat room
    newChatBtn.addEventListener('click', function() {
        var chatroomName = window.prompt("please input the name of the new chatroom", "");

        // check if the name is available
        if(chatroomName != null && chatroomName != '' && chatroomName != 'public'){
            
            // new a chat room
            var roomID = firebase.database().ref('chatroom').push({
                roomName : chatroomName,
                bgImgURL : ''
            }).key;
            
            // add the roomID in user's list
            firebase.database().ref('user/' + userID).push(roomID);
            
            chatroomRef = firebase.database().ref('chatroom/' + roomID);

            // send a null message
            var now = new Date();
            chatroomRef.child('content').push({
                email: user_email,
                message : '',
                date: {
                    month : now.getMonth() + 1,
                    day : now.getDate(),
                    hour : now.getHours(),
                    min : now.getMinutes() + 1
                }
            });
            changeChat(roomID);
            initChatList(roomID, chatroomName).then(()=>{
                updateChatList(roomID);
            });
        }

        // window.location.href = "index.html"
    });

    // update user name
    nameInputBtn.addEventListener('click', function(){
        var newUserName = document.getElementById('nameInput').value;
        firebase.database().ref('user2/' + user2ID).update({userName:newUserName});
        createNotification('your name has been changed successfully!')
        window.location.href = "index.html";
    });
    
    // update user headshot
    headshotInputBtn.addEventListener('click', function(){
        var File = document.getElementById('headshotInput').files[0];
        
        var photoRef = firebase.storage().ref().child(Date.now() + File.name);
        photoRef.put(File).then(snapshot=>{
            photoRef.getDownloadURL().then(url=>{
                firebase.database().ref('user2/' + user2ID).update({headshot:url});
                createNotification('your headshot has been changed successfully!');
                window.location.href = "index.html";
            });
        });
    });

    // upload chat background img
    document.getElementById('upload').addEventListener('change', function(){
        var File = document.getElementById('upload').files[0];
        console.log(File.name);

        var photoRef = firebase.storage().ref().child(Date.now() + File.name);
        photoRef.put(File).then(snapshot=>{
            photoRef.getDownloadURL().then(url=>{
                chatroomRef.update({ bgImgURL: url });
            });
        });
    });
    
    // update public chatroom
    chatroomRef.once('value', snapshot=>{
        if(!snapshot.child('bgImgURL').exists()){
            console.log('set default bg')
            chatroomRef.update({ bgImgURL: '' });
        }
    })
    changeChat('public');
};


// change the chatroom that is tracked
// function updateChat(chatroomRef){

//     chatroomRef.on('value', chatListener = function(snapshot){
//         chatroomContent.innerHTML = "";            
//         snapshot.forEach(element => {
//             if(element.val().message != ''){
//                 chatroomContent.innerHTML += 
//                     "<div class='p-1 mb-3'>" +
//                         "<p class='w-50 m-1 text-info'>" + element.val().email + "</p>" +
//                         "<span class='rounded py-1 px-3' style='background-color:white'>" + element.val().message + "</span>" +
//                     "</div>";
//             }
//         });
//     });
// }

function changeChat(roomID){
    var i = 0;
    var strarr = [];

    // turn off old listener
    console.log('go to');
    console.log(roomID);
    chatroomRef.child('content').off('value', chatListener);
    chatroomRef.child('bgImgURL').off('value', chatListener2);

    // switch to new room
    chatroomRef = firebase.database().ref('chatroom/' + roomID);

    // trace content
    chatroomRef.child('content').on('value', chatListener = function(snapshot){
        
        chatroomContent.innerHTML = "";

        snapshot.forEach(function(element){

            if(element.val().message != ''){
                console.log('inin3')   
                console.log(element.val().email)          
                
                console.log(i);
                // promise.push
                strarr.push( 
                    getUserID(element.val().email).then(function(id){    
                    console.log('this is id')            
                    console.log(id)            
                    return firebase.database().ref('user2/' + id).once('value').then(function(snapshot2){
                        console.log('inin4')            
                        console.log(i);    
                        return  "<div class='p-2 mb-2 d-flex overflow-auto' style='width:50%'>" +
                                    "<div class='py-1 pl-2 message1'>"+
                                        "<img src='" + snapshot2.val().headshot + "' width='50px' height='50px' style='border-radius: 50%'>"+
                                    "</div>"+
                                    "<div class='py-1 pr-2 message2'>"+
                                        "<p class='w-50 m-1 text-info'>" + snapshot2.val().userName + "</p>" +
                                        "<p class='rounded m-0 px-3' style='background-color:white'>" + element.val().message +
                                        "</p>" +
                                    "</div>" +
                            "</div>" ;                            
                    })
                })
                )
            }
        })
        
        chatroomContent.innerHTML = '';
        Promise.all(strarr)
        .then(v=> {
            chatroomContent.innerHTML = v.join(' ');        
            strarr.length = 0;
        })
        console.log('--------')
    });

    // trace bg img
    chatroomRef.child('bgImgURL').on('value', chatListener2 = function(snapshot){
        if(snapshot.val() != ''){
            console.log('have bgimg');
            chatroomContent.style.backgroundImage = "url('" + snapshot.val() + "')";
        }
        else{
            console.log('using default bgimg');
            chatroomContent.style.backgroundImage = "url('../img/chatroom.jpg')";
        }
    });
}

function updateChatList(roomID){
    // console.log('start');
    firebase.database().ref('chatroom/' + roomID + '/content').limitToLast(1).on('value', 
        lastMessage=>{
            lastMessage.forEach(messageElement=>{
                // var tmp = messageElement.val().email.substr(0, messageElement.val().email.length - 10);

                getUserID(messageElement.val().email).then(id=>{
                    firebase.database().ref('user2/' + id).once('value',  snapshot=>{

                        document.getElementById(roomID).innerHTML = 
                            messageElement.val().date.month + '/' + messageElement.val().date.day + ' ' + 
                            messageElement.val().date.hour + ':' + messageElement.val().date.min;
                        document.getElementById(roomID+'2').innerHTML = 
                            '<span class="text-primary">' + snapshot.val().userName + ':</span> ' + messageElement.val().message;  
                    })
                });

                // document.getElementById(roomID).innerHTML = 
                //     messageElement.val().date.month + '/' + messageElement.val().date.day + ' ' + 
                //         messageElement.val().date.hour + ':' + messageElement.val().date.min;
                // document.getElementById(roomID+'2').innerHTML = 
                //     '<span class="text-primary">' + tmp + ':</span> ' + messageElement.val().message;                                     
            });
        }
    );

}

function initChatList(roomID, roomName){
    return new Promise(function(resolve, reject){
        // console.log(roomID); console.log(roomName);
        firebase.database().ref('chatroom/' + roomID + '/content').limitToLast(1).once('value', 
            lastMessage=>{

                lastMessage.forEach(messageElement=>{
                    
                    // var tmp = messageElement.val().email.substr(0, messageElement.val().email.length - 10);
    
                    getUserID(messageElement.val().email).then(id=>{
                        // console.log(id);
    
                        firebase.database().ref('user2/' + id).once('value',  snapshot=>{

                            chatroomList.innerHTML += 
                                '<div class="list-group-item list-group-item-action d-flex flex-column justify-content-betwee p-0" onclick="changeChat(\'' + roomID + '\')">' +
                                    '<div class="d-flex justify-content-between">' +
                                        '<p class="font-weight-bold p-1 m-0">' + roomName + '</p>' +
                                        '<small id="' + roomID + '" class="p-1">' + messageElement.val().date.month + '/' + messageElement.val().date.day + '</small>' +
                                    '</div>' +
                                    '<p id="' + roomID + '2" class="m-0 p-1 text-truncate"><span class="text-primary">' + snapshot.val().userName + ':</span> ' + messageElement.val().message + '</p>' +
                                '</div>'
                            // resolve();
                        })

                    });
    
                    // chatroomList.innerHTML += 
                    //                 '<div class="list-group-item list-group-item-action d-flex flex-column justify-content-betwee p-0" onclick="changeChat(\'' + roomID + '\')">' +
                    //                     '<div class="d-flex justify-content-between">' +
                    //                         '<p class="font-weight-bold p-1 m-0">' + roomName + '</p>' +
                    //                         '<small id="' + roomID + '" class="p-1">' + messageElement.val().date.month + '/' + messageElement.val().date.day + '</small>' +
                    //                     '</div>' +
                    //                     '<p id="' + roomID + '2" class="m-0 p-1 text-truncate"><span class="text-primary">' + tmp + ':</span> ' + messageElement.val().message + '</p>' +
                    //                 '</div>'
    
                    
                })
        });
    })
}


function getUserID(email){
    var key;

    return new Promise(function(resolve, reject){
        firebase.database().ref('user2/').orderByChild("email").equalTo(email).once('value', (snapshot)=>{
            
            // console.log('---------');

            if(snapshot.exists()){
                snapshot.forEach(element=>{
                    key = element.key;
                    console.log(element.val());
                })
                console.log(key);
                resolve(key);
            }
            else
               reject('failed to get id');

            // console.log('---------');
    
        });

    })
}

function createNotification(content){
        // chrome notification
        var notifyConfig = { body: content };
        if (Notification.permission === 'default' || Notification.permission === 'undefined') {
            Notification.requestPermission(function(permission) {
                if(permission == 'granted'){
                    var notification = new Notification('Hi there!', notifyConfig);
                }
            });
        }
        else if (Notification.permission === 'granted'){
            // console.log('in4');
            var notification = new Notification('Hi there!', notifyConfig);
        }
}
